FROM maven:3.8.2-jdk-8
WORKDIR /app
COPY . .
ENV DB_HOST "localhost"
RUN mvn clean install
CMD mvn spring-boot:run